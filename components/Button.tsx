import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, TouchableOpacityProps, View } from 'react-native';
import { TEXT_VARIANTS, VARIANTS, VariantColor } from './Themed';



interface ButtonProps extends TouchableOpacityProps {
    variant?: VariantColor;
}

export default function Button({ children, style, variant = "gray", ...props }: ButtonProps) {
    const styles = useStyles(variant);

    return (
        <TouchableOpacity style={[styles.button, style]} {...props}>
            {typeof children === "string" ? <Text style={styles.text}>{children}</Text> : children}
        </TouchableOpacity>
    )
}


const useStyles = (variant: VariantColor) => StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: VARIANTS[variant],
        padding: 20,
    },
    text: {
        color: TEXT_VARIANTS[variant],
        fontWeight: "bold"
    }
});
