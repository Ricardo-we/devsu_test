import { StyleProp, StyleSheet, TextInputProps, ViewStyle } from "react-native";
import { TEXT_VARIANTS, Text, TextInput, View } from "./Themed";

export interface CustomTextInputProps extends TextInputProps {
  error?: string;
  containerStyle?: StyleProp<ViewStyle>;
  disabled?: boolean;
}

export function FormInput({
  style,
  error,
  containerStyle,
  disabled,
  ...props
}: CustomTextInputProps) {
  return (
    <View testID="form-input" style={[styles.container, containerStyle]}>
      <Text style={{ color: TEXT_VARIANTS.black, fontWeight: "bold" }}>
        {props.placeholder}
      </Text>
      <TextInput
        testID="form-input-text-input"
        style={[
          {
            borderColor: error ? "red" : "gray",
            borderWidth: 1,
            borderRadius: 5,
            padding: 10,
            width: "100%",
            backgroundColor: disabled ? "#f6f6f6" : "white",
          },
          style,
        ]}
        editable={!disabled}
        {...props}
      />
      {error && <Text style={styles.errorText}>{error}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  errorText: {
    color: "red",
    fontSize: 12,
  },
});
