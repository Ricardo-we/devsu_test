import React from 'react'
import { Text, View } from '../Themed'
import { Pressable, StyleSheet } from 'react-native'
import { AntDesign } from '@expo/vector-icons';

interface FinanceProductItemProps {
  title?: string;
  subtitle?: string;
  onPressArrow?: () => any;
}


export default function FinanceProductItem({ title, subtitle, onPressArrow }: FinanceProductItemProps) {
  return (
    <View style={styles.rowContainer}>
      <View style={styles.columnContainer}>
        <Text style={styles.darkText}>
          {title}
        </Text>

        <Text style={styles.lightText}>
          {subtitle}
        </Text>
      </View>


      <Pressable testID='arrow-icon' onPress={onPressArrow} >
        <AntDesign name="right" size={24} color="gray" />
      </Pressable>

    </View>
  )
}


const styles = StyleSheet.create({
  rowContainer: {
    // flex: 1,
    display: "flex",

    alignItems: 'center',
    flexDirection: "row",
    justifyContent: "space-between",
    // border: "1px solid #000",
    borderWidth: 1,
    borderColor: "gray",
    width: "90%",
    padding: 9,
    borderRadius: 5,
  },
  columnContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start"
  },
  lightText: {
    color: "gray",
    fontSize: 12,
    fontWeight: "300",
  },
  darkText: {
    fontWeight: "bold",
    fontSize: 15,
  },
});