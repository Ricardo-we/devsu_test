/**
 * Learn more about Light and Dark modes:
 * https://docs.expo.io/guides/color-schemes/
 */

import {
  Text as DefaultText,
  View as DefaultView,
  TextInputProps,
  TextInput as DefaultTextInput,
  ImageProps,
  Image as DefaultImage,
  ImageSourcePropType,
  Button as DefaultButton,
  ButtonProps
} from 'react-native';

import Colors from '@/constants/Colors';
import { useColorScheme } from './useColorScheme';
import { useState } from 'react';

type ThemeProps = {
  lightColor?: string;
  darkColor?: string;
};

export const VARIANTS = {
  gray: "#e9ecf3",
  error: "#d50707",
  yellow: "#ffdd00",
  black: "#000000"
}

export const TEXT_VARIANTS = {
  gray: "#525a41",
  error: "white",
  yellow: "#525a41",
  black: "#000000"
}

export type VariantColor = keyof typeof VARIANTS;
export type TextProps = ThemeProps & DefaultText['props'];
export type ViewProps = ThemeProps & DefaultView['props'];


export function useThemeColor(
  props: { light?: string; dark?: string },
  colorName: keyof typeof Colors.light & keyof typeof Colors.dark
) {
  const theme = useColorScheme() ?? 'light';
  const colorFromProps = props[theme];

  if (colorFromProps) {
    return colorFromProps;
  } else {
    return Colors[theme][colorName];
  }
}

export function Text(props: TextProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const color = useThemeColor({ light: lightColor, dark: darkColor }, 'text');

  return <DefaultText style={[{ color }, style]} {...otherProps} />;
}

export function View(props: ViewProps) {
  const { style, lightColor, darkColor, ...otherProps } = props;
  const backgroundColor = useThemeColor({ light: lightColor, dark: darkColor }, 'background');

  return <DefaultView style={[{ backgroundColor }, style]} {...otherProps} />;
}




export function TextInput({ style, ...props }: TextInputProps) {
  return <DefaultTextInput
    style={[{
      borderColor: 'gray',
      borderWidth: 1,
      borderRadius: 5,
      padding: 10,
    }, style]}
    {...props}
  />
}


interface CustomImageProps extends ImageProps {
  fallbackImage?: ImageSourcePropType;
}

export function Image({ source, onError, ...props }: CustomImageProps) {
  const [hasError, setHasError] = useState(false);

  return <DefaultImage
    {...props}

    source={!hasError || !props.fallbackImage ? source : props.fallbackImage}
    onError={(e) => {
      setHasError(true);
      onError && onError(e);
    }}
  />
}


