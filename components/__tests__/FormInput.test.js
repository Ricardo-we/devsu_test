import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import { FormInput } from "../FormInput"; // Assuming correct path
import { TextInput, VARIANTS, View } from "../Themed";
import renderer from "react-test-renderer";

describe("FormInput", () => {
  it(`renders correctly`, () => {
    const tree = renderer
      .create(<FormInput placeholder="Enter Value" />)
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders correctly with placeholder", () => {
    const { getByText } = render(<FormInput placeholder="Enter Value" />);

    expect(getByText("Enter Value")).toBeTruthy();
  });

  it("renders with error message", () => {
    const { getByText } = render(
      <FormInput placeholder="Enter Value" error="This field is required" />
    );

    expect(getByText("Enter Value")).toBeTruthy();
    expect(getByText("This field is required")).toBeTruthy();
  });

  it("handles input changes", async () => {
    const onChangeTextMock = jest.fn();
    const { getByPlaceholderText } = render(
      <FormInput
        placeholder="Enter Value"
        value="1"
        onChangeText={onChangeTextMock}
        error="This field is required"
      />
    );

    expect(getByPlaceholderText("Enter Value").props.value).toBe("1");
    fireEvent.changeText(getByPlaceholderText("Enter Value"), "guava");

    expect(onChangeTextMock).toHaveBeenCalled();
    // expect(getByPlaceholderText("Enter Value").props.value).toBe("guava");
  });

  it("disables input field when disabled prop is true", () => {
    const { getByPlaceholderText } = render(
      <FormInput placeholder="Enter Value" disabled />
    );

    expect(getByPlaceholderText("Enter Value").props.editable).toBe(false);
  });

  it("applies border color based on error prop", () => {
    const { getByPlaceholderText } = render(
      <FormInput placeholder="Enter Value" />
    );
    const input = getByPlaceholderText("Enter Value");

    expect(input.props.style[0].borderColor).toBe("gray"); // Default

    const rerendered = render(
      <FormInput placeholder="Enter V" error="This field is required" />
    );
    const errorInput = rerendered.getByPlaceholderText("Enter V");

    expect(errorInput.props.style[1][0].borderColor).toBe("red");
  });

});
