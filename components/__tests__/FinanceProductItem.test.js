import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import FinanceProductItem from "../F1/FinanceProductItem";
import renderer from "react-test-renderer";

describe("FinanceProductItem", () => {
  it(`renders correctly`, () => {
    const tree = renderer
      .create(
        <FinanceProductItem
          title="Test Title"
          subtitle="Test Subtitle"
          onPressArrow={() => {}}
        />
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders title and subtitle correctly", () => {
    const { getByText } = render(
      <FinanceProductItem
        title="Test Title"
        subtitle="Test Subtitle"
        onPressArrow={() => {}}
      />
    );

    expect(getByText("Test Title")).toBeTruthy(); // Check if title is rendered
    expect(getByText("Test Subtitle")).toBeTruthy(); // Check if subtitle is rendered
  });

  it("calls onPressArrow when arrow icon is pressed", () => {
    const onPressArrow = jest.fn();
    const { getByTestId } = render(
      <FinanceProductItem
        title="Test Title"
        subtitle="Test Subtitle"
        onPressArrow={onPressArrow}
      />
    );

    fireEvent.press(getByTestId("arrow-icon"));
    expect(onPressArrow).toHaveBeenCalled();
  });
});
