import renderer from 'react-test-renderer';
import EditScreenInfo from '../EditScreenInfo';

describe("EditScreenInfo", () => {
  it("renders correctly", () => {
    const tree = renderer.create(<EditScreenInfo />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
