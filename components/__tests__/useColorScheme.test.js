import { useColorScheme } from '../useColorScheme.web';

describe('useColorScheme', () => {
  it('should return "light"', () => {
    const result = useColorScheme();
    expect(result).toBe('light');
  });
});