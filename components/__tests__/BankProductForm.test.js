import React from "react";
import { render, fireEvent, act, waitFor } from "@testing-library/react-native";
import BankProductForm from "../forms/BankProductForm";

const initialValues = {
  id: "123",
  name: "Test Product",
  description: "This is a test product",
  logo: "test-logo.png",
  date_release: "01/01/2023",
  date_revision: "01/01/2024",
};

describe("BankProductForm", () => {
  it("renders correctly with initial values", () => {
    const { getByPlaceholderText, getByText } = render(
      <BankProductForm
        initialValues={initialValues}
        onSubmit={jest.fn()}
        onCancel={jest.fn()}
      />
    );

    expect(getByText("Nuevo producto bancario")).toBeTruthy();
    expect(getByPlaceholderText("ID").props.value).toBe("123");
    expect(getByPlaceholderText("Nombre del producto").props.value).toBe(
      "Test Product"
    );
    expect(getByPlaceholderText("Descripción").props.value).toBe(
      "This is a test product"
    );
    expect(getByPlaceholderText("Logo").props.value).toBe("test-logo.png");
    expect(getByPlaceholderText("Fecha de liberación").props.value).toBe(
      "01/01/2023"
    );
    expect(getByPlaceholderText("Fecha de revisión").props.value).toBe(
      "01/01/2024"
    );
  });


  it("resets the form correctly", () => {
    const { getByText, getByPlaceholderText } = render(
      <BankProductForm
        initialValues={initialValues}
        onSubmit={jest.fn()}
        onCancel={jest.fn()}
      />
    );

    fireEvent.changeText(getByPlaceholderText("ID"), "456");
    fireEvent.changeText(
      getByPlaceholderText("Nombre del producto"),
      "Updated Product"
    );
    fireEvent.press(getByText("Reiniciar"));

    expect(getByPlaceholderText("ID").props.value).toBe("123");
    expect(getByPlaceholderText("Nombre del producto").props.value).toBe(
      "Test Product"
    );
  });

  it("calculates the date_revision correctly", () => {
    const { getByPlaceholderText } = render(
      <BankProductForm
        initialValues={{
          ...initialValues,
          date_release: "01/01/2023",
        }}
        onSubmit={jest.fn()}
        onCancel={jest.fn()}
      />
    );

    expect(getByPlaceholderText("Fecha de revisión").props.value).toBe(
      "01/01/2024"
    );
  });

  it("disables the ID input when initialValues.id is provided", () => {
    const { getByPlaceholderText } = render(
      <BankProductForm
        initialValues={initialValues}
        onSubmit={jest.fn()}
        onCancel={jest.fn()}
      />
    );

    expect(getByPlaceholderText("ID").props.editable).toBe(false);
  });
});
