import React from "react";
import { render, fireEvent } from "@testing-library/react-native";
import BottomModal from "../BottomModal";
import { Text } from "react-native";

import renderer from "react-test-renderer";

describe("BottomModal", () => {
  it(`renders correctly`, () => {
    const tree = renderer
      .create(
        <BottomModal isVisible={true} title="Test Modal">
          <Text>Modal Content</Text>
        </BottomModal>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders modal with title and children when visible", () => {
    const onClose = jest.fn();
    const { getByText, queryByText } = render(
      <BottomModal isVisible={true} onClose={onClose} title="Test Modal">
        <Text>Modal Content</Text>
      </BottomModal>
    );

    expect(getByText("Test Modal")).toBeTruthy(); // Check if title is rendered
    expect(getByText("Modal Content")).toBeTruthy(); // Check if children are rendered
  });

  it("does not render modal when not visible", () => {
    const onClose = jest.fn();
    const { queryByText } = render(
      <BottomModal isVisible={false} onClose={onClose} title="Test Modal">
        <Text>Modal Content</Text>
      </BottomModal>
    );

    expect(queryByText("Test Modal")).toBeNull(); // Ensure title is not rendered
    expect(queryByText("Modal Content")).toBeNull(); // Ensure children are not rendered
  });
});
