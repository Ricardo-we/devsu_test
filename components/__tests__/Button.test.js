import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import Button from '../Button';
import { VARIANTS } from '../Themed';

describe('Button', () => {
  it('renders children correctly', () => {
    const { getByText } = render(<Button>Press Me</Button>);
    expect(getByText('Press Me')).toBeTruthy();
  });

4
  it('renders with specified variant', () => {
    const { getByTestId } = render(<Button testID="button-test" variant="black">Press Me</Button>);
    const button = getByTestId('button-test');

    expect(button.props.style.backgroundColor).toBe(VARIANTS.black); 
    expect(button.children[0].props.children).toBe('Press Me');
  });
  it('renders with specified variant', () => {
    const { getByTestId } = render(<Button testID="button-test" variant="yellow">Press Me</Button>);
    const button = getByTestId('button-test');

    expect(button.props.style.backgroundColor).toBe(VARIANTS.yellow); 
    expect(button.children[0].props.children).toBe('Press Me');
  });

  it('calls onPress prop when pressed', () => {
    const onPressMock = jest.fn();
    const { getByTestId } = render(<Button testID="button-test" onPress={onPressMock}>Press Me</Button>);
    const button = getByTestId('button-test');

    fireEvent.press(button);
    expect(onPressMock).toHaveBeenCalled();
  });
});
