import AppBar from '../AppBar';
import renderer from 'react-test-renderer';

describe("Appbar", () => {
  it(`renders correctly`, () => {
    const tree = renderer
      .create(
        <AppBar/>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
