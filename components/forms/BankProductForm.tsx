import { ScrollView, StyleSheet } from "react-native";
import Button from "../Button";
import { Text, TextInput, View } from "../Themed";
import { useFormik } from "formik";
import { BankProduct } from "@/types/BankProduct";

import { FormInput } from "../FormInput";
import { useCallback, useEffect } from "react";
import { useFocusEffect } from "expo-router";
import { BankProductValidations } from "@/utils/validators/BankProductValidations";
// import DateInput from "../DateInput";
import { useIsFocused } from "@react-navigation/native";
interface BankProductFormProps {
  initialValues?: Partial<BankProduct>;
  onSubmit: (values: BankProduct) => void;
  onCancel: () => void;
  title?: string;
  isFocused?: boolean;
}

export default function BankProductForm({
  initialValues,
  onSubmit,
  onCancel,
  title = "Nuevo producto bancario",
  isFocused,
}: BankProductFormProps) {
  const formik = useFormik({
    initialValues: { ...initialValues } as BankProduct,
    onSubmit: (values) => {
      onSubmit && onSubmit(values);
    },
    validationSchema: BankProductValidations,
    enableReinitialize: true,
  });

  useEffect(() => {
    if (formik.values.date_release?.split("/")?.length >= 3) {
      const [day, month, year] = formik.values.date_release
        ?.split("/")
        ?.map(Number) ?? [0, 0, 0];

      const releaseDate = new Date(year, month - 1, day);

      releaseDate.setFullYear(releaseDate.getFullYear() + 1);
      const formattedRevisionDate = `${
        releaseDate.getDate() < 10 ? "0" : ""
      }${releaseDate.getDate()}/${releaseDate.getMonth() < 9 ? "0" : ""}${
        releaseDate.getMonth() + 1
      }/${releaseDate.getFullYear()}`;

      formik.setFieldValue("date_revision", formattedRevisionDate);
    }
  }, [isFocused, formik.values.date_release]);

  const styles = useStyles();
  return (
    <ScrollView>
      <View style={styles.mainContainer}>
        <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 20 }}>
          {title}
        </Text>

        <FormInput
          value={formik.values.id as string}
          error={formik.errors.id}
          onChangeText={(value) => {
            formik.setFieldValue("id", value);
          }}
          style={styles.formInput}
          containerStyle={styles.formInputContainerStyle}
          placeholder="ID"
          disabled={!!initialValues?.id}
        />

        <FormInput
          value={formik.values.name}
          error={formik.errors.name}
          onChangeText={(value) => {
            formik.setFieldValue("name", value);
          }}
          style={styles.formInput}
          containerStyle={styles.formInputContainerStyle}
          placeholder="Nombre del producto"
        />

        <FormInput
          value={formik.values.description}
          error={formik.errors.description}
          onChangeText={(value) => formik.setFieldValue("description", value)}
          style={styles.formInput}
          placeholder="Descripción"
          containerStyle={styles.formInputContainerStyle}
        />

        <FormInput
          value={formik.values.logo}
          onChangeText={(value) => formik.setFieldValue("logo", value)}
          style={styles.formInput}
          placeholder="Logo"
          containerStyle={styles.formInputContainerStyle}
        />

        <FormInput
          value={formik.values.date_release}
          onChangeText={(value) => {
            formik.setFieldValue("date_release", value);
          }}
          style={styles.formInput}
          error={formik.errors.date_release}
          placeholder="Fecha de liberación"
          containerStyle={styles.formInputContainerStyle}
        />

        <FormInput
          value={formik.values.date_revision}
          onChangeText={(value) => formik.setFieldValue("date_revision", value)}
          style={styles.formInput}
          error={formik.errors.date_revision}
          disabled
          placeholder="Fecha de revisión"
          containerStyle={styles.formInputContainerStyle}
        />

        <Button
          variant="yellow"
          disabled={!formik.isValid}
          style={styles.button}
          onPress={formik.submitForm}
        >
          Enviar
        </Button>
        <Button style={styles.button} onPress={() => formik.resetForm()}>
          Reiniciar
        </Button>
      </View>
    </ScrollView>
  );
}

const useStyles = () =>
  StyleSheet.create({
    mainContainer: {
      width: "100%",
      height: "100%",
      padding: 20,
    },
    formInput: {
      // marginBottom: 10,
      padding: 10,
      //   backgroundColor: "white",
      borderRadius: 5,
    },
    formInputContainerStyle: {
      marginBottom: 15,
    },
    button: {
      marginTop: 10,
    },
  });
