import React from "react";
import { Text, View } from "./Themed";
import { FontAwesome6 } from "@expo/vector-icons";
import { StyleSheet } from "react-native";
import { StatusBar } from "expo-status-bar";

export default function AppBar() {
  return (
    <View style={styles.container}>
      <FontAwesome6 name="money-bill-1" size={15} color="black" />

      <Text style={styles.mainTitle}>
        BANCO
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  mainTitle: {
    fontSize: 15,
    fontWeight: "bold",
    color: "black",
    marginLeft: 5
  }
});
