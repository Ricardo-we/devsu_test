# Devsu test

## Technologies used
- Node 20
- Expo sdk 51.0.0
- Typescript 5.3.3
- React 18.2.0 

# Instructions
### Run project
To run the project you have to run the next commands:
`npm install`
`npx expo start` or `npm start`
Then select the emulator in which you want to run the project

### Run tests
To run test you can use this command `npm test` or use jest to run them.

