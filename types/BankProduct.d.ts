import { BaseModel } from "./BaseModel";

export interface BankProduct extends BaseModel {
    name: string;
    description: string;
    logo: string;
    date_release: string;
    date_revision: string;
}