import appSettings from "@/app-settings";
import { BaseService } from "./BaseService";
import { BankProduct } from "@/types/BankProduct";

export class BankProductsService extends BaseService {

    constructor(){
        super(appSettings.apiEndpoints.bpProducts)
    }

    async getProducts() {
        const response = await fetch(this.getEndpoint(), this.getDefaultHeaders());
        return (await response.json())?.data as Array<BankProduct>;
    }


    async getProductById(id?: string | number){
        const response = await fetch(`${this.getEndpoint()}/${id}`, this.getDefaultHeaders());
        return (await response.json())?.data as BankProduct;
    }

    async createProduct(product: BankProduct){
        const response = await fetch(this.getEndpoint(), {
            method: "POST",
            headers: this.getDefaultHeaders(),
            body: JSON.stringify(product)
        });

        return (await response.json())?.data as BankProduct;
    }


    async updateProduct(product: BankProduct){
        const id = product.id;
        delete product.id;
        const response = await fetch(`${this.getEndpoint()}/${id}`, {
            method: "PUT",
            headers: this.getDefaultHeaders(),
            body: JSON.stringify(product)
        });

        return (await response.json())?.data as BankProduct;
    }

    async deleteProduct(id: string | number){
        const response = await fetch(`${this.getEndpoint()}/${id}`, {
            method: "DELETE",
            headers: this.getDefaultHeaders()
        });

        return (await response.json())?.data as BankProduct;
    }

}