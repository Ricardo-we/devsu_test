import appSettings from "@/app-settings";

export class BaseService {
    private API_URL: string = appSettings.apiBaseUrl;
    private endpoint: string = "";
    private headers: {} = {};
    
    constructor(endpoint: string){
        this.endpoint = this.API_URL + endpoint;
        this.headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    }

    getEndpoint(){
        return this.endpoint;
    }

    getDefaultHeaders(){
        return this.headers;
    }
}