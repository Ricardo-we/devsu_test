import BankProductForm from "@/components/forms/BankProductForm";
import { BankProductsService } from "@/services/BpProductsService";
import { parseDateDDMMYYYY } from "@/utils/date.utils";
import { useIsFocused } from "@react-navigation/native";
import { router, useLocalSearchParams } from "expo-router";
import React, { useCallback, useEffect, useLayoutEffect } from "react";
import { Alert } from "react-native";

export default function index() {
  const bankProductsService = new BankProductsService();
  const { action, ...bankProduct } = useLocalSearchParams();
  const isFocused = useIsFocused();

  const parseDate = (date: string | Date) => {
    return new Date(date)?.toLocaleDateString("es-ES", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
    });
  };

  return (
    <BankProductForm
      initialValues={{
        ...bankProduct,
        ...(bankProduct.date_release && bankProduct.date_revision
          ? {
              date_release: parseDate(bankProduct.date_release as string),
              date_revision: parseDate(bankProduct.date_revision as string),
            }
          : {}),
      }}
      isFocused={isFocused}
      onCancel={() => {}}
      onSubmit={(formData) => {
        const data = { ...formData };
        data.date_release = parseDateDDMMYYYY(data.date_release).toISOString();
        data.date_revision = parseDateDDMMYYYY(
          data.date_revision
        ).toISOString();

        if (action === "add") {
          return bankProductsService
            .createProduct(data)
            .then((response) => {
              if (!response)
                throw new Error(
                  "No se pudo editar el producto bancario, revise que todos los datos sean correctos y no sea un ID repetido"
                );
              router.navigate("/F1");
              // router.back();
            })
            .catch(() =>
              Alert.alert("Error", "No se pudo crear el producto bancario")
            );
        }

        return bankProductsService
          .updateProduct(data)
          .then((response) => {
            if (!response)
              throw new Error("No se pudo editar el producto bancario");
            router.navigate("/F1");
          })
          .catch(() =>
            Alert.alert("Error", "No se pudo editar el producto bancario")
          );
      }}
    />
  );
}
