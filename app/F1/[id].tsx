import BottomModal from "@/components/BottomModal";
import Button from "@/components/Button";
import { Image, Text, View } from "@/components/Themed";
import { BankProductsService } from "@/services/BpProductsService";
import { BankProduct } from "@/types/BankProduct";
import { router, useLocalSearchParams } from "expo-router";
import React, { useState } from "react";
import { StyleSheet, useColorScheme } from "react-native";

const BankProductItem = ({
  title,
  value,
}: {
  title: string;
  value?: string | number;
}) => {
  const styles = useStyles();

  return (
    <View style={styles.rowContainer}>
      <Text style={styles.lightText}>{title}</Text>
      <Text style={[styles.lightText, { fontWeight: "bold", maxWidth: 160 }]}>
        {value}
      </Text>
    </View>
  );
};

function BankProductDetailView() {
  const bankProduct: Partial<BankProduct> = useLocalSearchParams();
  const bankProductsService = new BankProductsService();
  const theme = useColorScheme();
  const styles = useStyles();
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  const onDeleteBankProduct = () => {
    bankProductsService
      .deleteProduct(bankProduct.id as string)
      .then(() => router.replace({ pathname: "/F1" }))
      .catch(console.error);
  };

  return (
    <View style={styles.mainContainer}>
      <BottomModal
        isVisible={isModalVisible}
        onClose={() => setIsModalVisible(false)}
        title={`¿Estás seguro de eliminar el producto ${bankProduct.name}?`}
      >
        <Button
          variant="yellow"
          style={{ marginVertical: 15 }}
          onPress={onDeleteBankProduct}
        >
          Confirmar
        </Button>
        <Button variant="gray" onPress={() => setIsModalVisible(false)}>
          Cancelar
        </Button>
      </BottomModal>

      <View style={styles.titleContainer}>
        <Text style={{ fontSize: 25, fontWeight: "bold" }}>
          ID: {bankProduct.id}
        </Text>
        <Text style={styles.lightText}>Informacion extra</Text>
      </View>

      <View style={{ padding: 20 }}>
        <BankProductItem title="Nombre" value={bankProduct?.name} />
        <BankProductItem title="Descripción" value={bankProduct?.description} />
        <BankProductItem
          title="Fecha liberación"
          value={new Date(
            bankProduct?.date_release ?? ""
          )?.toLocaleDateString()}
        />
        <View style={{}}>
          <Text style={styles.lightText}>Logo</Text>
          <Image
            source={{ uri: bankProduct?.logo }}
            // source={require("../../assets/images/card-placeholder.png")}
            style={{ width: 100, height: 100, margin: "auto" }}
            fallbackImage={require("../../assets/images/card-placeholder.png")}
          />
        </View>
        <BankProductItem
          title="Fecha de revisión"
          value={new Date(
            bankProduct?.date_revision ?? ""
          )?.toLocaleDateString()}
        />
      </View>

      <View style={{ marginTop: "auto", gap: 10 }}>
        <Button
          onPress={() =>
            router.push({ pathname: "bank-product/edit", params: bankProduct })
          }
        >
          Editar
        </Button>
        <Button variant="error" onPress={() => setIsModalVisible(true)}>
          Eliminar
        </Button>
      </View>
    </View>
  );
}

const useStyles = () =>
  StyleSheet.create({
    mainContainer: {
      // marginHorizontal: 10
      width: "100%",
      height: "100%",
      padding: 20,
    },
    titleContainer: {},
    lightText: {
      color: "gray",
      fontSize: 16,
    },
    rowContainer: {
      // marginVertical: 10,
      flexDirection: "row",
      justifyContent: "space-between",
      margin: 0,
      padding: 0,
      width: "100%",
    },
  });
export default BankProductDetailView;
