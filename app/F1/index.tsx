import appSettings from "@/app-settings";
import AppBar from "@/components/AppBar";
import Button from "@/components/Button";
import FinanceProductItem from "@/components/F1/FinanceProductItem";
import { Text, TextInput, View } from "@/components/Themed";
import { BankProductsService } from "@/services/BpProductsService";
import { BankProduct } from "@/types/BankProduct";
import { router, useFocusEffect } from "expo-router";
import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { ScrollView } from "react-native";
import { Dimensions } from "react-native";

export default function index({}) {
  const [search, setSearch] = React.useState<string>("");
  const bpProductsService = new BankProductsService();
  const [products, setProducts] = React.useState<BankProduct[]>([]);

  useFocusEffect(() => {
    bpProductsService
      .getProducts()
      .then((data) => {
        setProducts(data);
      })
      .catch(console.error);
  });

  return (
    <ScrollView>
      <View style={styles.mainContainer}>
        <TextInput
          placeholder="Search..."
          style={{
            marginTop: 10,
            width: "90%",
            marginBottom: 30,
          }}
          value={search}
          onChangeText={(text) => setSearch(text)}
        />

        {products
          .filter((item) =>
            Object.values(item).some((value) => value.includes(search))
          )
          .map((product, index) => (
            <FinanceProductItem
              key={index}
              title={product?.name}
              subtitle={`ID: ${product?.id}`}
              onPressArrow={() => {
                router.push({
                  pathname: `/F1/${product?.id}`,
                  params: { ...product, id: undefined } as any,
                });
              }}
            />
          ))}

        <Button
          onPress={() => router.push("/bank-product/add")}
          variant="yellow"
          style={{ width: "90%", marginTop: "auto" }}
        >
          Agregar
        </Button>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-start",
    width: "100%",
    height: Dimensions.get("screen").height - 200,
    margin: "auto",
    padding: 0,
  },
  searchInput: {},
});
