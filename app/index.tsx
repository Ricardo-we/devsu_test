import AppBar from '@/components/AppBar'
import { Text, View } from '@/components/Themed'
import { Redirect } from 'expo-router'
import React from 'react'
import { StyleSheet, TextInput } from 'react-native'
import F1View from './F1';

export default function AppEntry({ }) {
    return (
        <F1View/>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
    },
    searchInput: {},
});
