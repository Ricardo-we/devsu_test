export function parseDateDDMMYYYY(dateString: string) {
    // Dividir la cadena de fecha en día, mes y año
    const [day, month, year] = dateString.split('/').map(Number);
    
    // Crear un nuevo objeto Date (los meses son zero-indexed en JavaScript)
    return new Date(year, month - 1, day);
  }