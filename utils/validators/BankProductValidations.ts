import { parseDateDDMMYYYY } from "@/utils/date.utils";
import * as yup from "yup";

const DATE_REGEX = /^\d{2}\/\d{2}\/\d{4}$/;
export const BankProductValidations =  yup.object().shape({
    id: yup
      .string()
      .required("ID es requerido")
      .min(3, "El campo ID debe tener al menos 3 caracteres")
      .max(10, "El campo ID debe tener como máximo 10 caracteres"),
    name: yup
      .string()
      .required("El nombre es requerido")
      .min(5, "El campo nombre debe tener al menos 5 caracteres")
      .max(100, "El campo nombre debe tener como máximo 100 caracteres"),
    description: yup
      .string()
      .required("La descripción es requerida")
      .min(10, "El campo descripción debe tener al menos 10 caracteres")
      .max(200, "El campo descripción debe tener como máximo 200 caracteres"),
    date_release: yup
      .string()
      .required("La fecha de liberación es requerida")
      .matches(DATE_REGEX, "Formato de fecha inválido. Use DD/MM/YYYY")
      .test(
        "is-greater-than-today",
        "La fecha de liberación debe ser mayor o igual a la fecha actual",
        function (value) {
          if (!value) return false; // Si no hay valor, no pasa la validación de required
          const releaseDate = parseDateDDMMYYYY(value);
          const today = new Date();
          today.setHours(0, 0, 0, 0); // Ignorar la hora para comparar solo fechas
          return releaseDate >= today;
        }
      ),
    logo: yup.string().required("El logo es requerido"),
    date_revision: yup
      .string()
      .required("La fecha de revisión es requerida")
      .matches(DATE_REGEX, "Formato de fecha inválido. Use DD/MM/YYYY"),
  })