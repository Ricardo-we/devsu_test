
export default {
    apiBaseUrl: process.env.API_URL ?? "http://192.168.1.15:3002",
    apiVersion: 'v1',
    apiEndpoints: {
        bpProducts: "/bp/products",
    }
}
